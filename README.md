# Torres Hanoi Simulación

Éste código desarrollado en C y utilizando ncurses para la animación y colores, simula el tradicional juego de torres de Hanoi.

El juego, en su forma más tradicional, consiste en tres varillas verticales. En una de las varillas se apila un número indeterminado de discos (elaborados de madera) que determinará la complejidad de la solución, por regla general se consideran ocho discos. Los discos se apilan sobre una varilla en tamaño decreciente. No hay dos discos iguales, y todos ellos están apilados de mayor a menor radio en una de las varillas, quedando las otras dos varillas vacantes. El juego consiste en pasar todos los discos de la varilla ocupada (es decir la que posee la torre) a una de las otras varillas vacantes. Para realizar este objetivo, es necesario seguir tres simples reglas:

- Sólo se puede mover un disco cada vez.
- Un disco de mayor tamaño no puede descansar sobre uno más pequeño que él mismo.
- Sólo puedes desplazar el disco que se encuentre arriba en cada torre.

From : https://www.geogebra.org/m/NqyWJVra

![alt tag](./1.png)

## Requerimientos para Fedora

```
sudo dnf -y install dkms gcc make bzip2 cmake ncurses-devel
```

## Compilar desde terminal

```
gcc -Wall -lncurses -o HanoiExec Hanoi.c
```

## Ejecutar

```
./HanoiExec
```

